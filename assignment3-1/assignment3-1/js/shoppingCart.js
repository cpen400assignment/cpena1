/* 
CPEN 400 Assignment #2
Author: Brandon Nam, Shariq Aziz
Date: Oct. 15, 2015
*/

//var products = {Box1: {'price': "10", 'quantity': "10"},
//                Box2: {'price': "5", 'quantity': "10"},
//                Clothes1: {'price': "20", 'quantity': "10"},
//                Clothes2: {'price': "30", 'quantity': "10"},
//                Jeans: {'price': "50", 'quantity': "10"},
//                Keyboard: {'price': "20", 'quantity': "10"},
//                KeyboardCombo: {'price': "40", 'quantity': "10"},
//                Mice: {'price': "20", 'quantity': "10"},
//                PC1: {'price': "350", 'quantity': "10"},
//                PC2: {'price': "400", 'quantity': "10"},
//                PC3: {'price': "300", 'quantity': "10"},
//                Tent: {'price': "100", 'quantity': "10"}
//};

var products;

// AJAX REQUEST TO GET PRODUCTS

var count = 0;
function request(callback) {
    var x = new XMLHttpRequest();
    x.open("GET", "https://cpen400a.herokuapp.com/products");
    x.send();
    x.onload = function() {
        if (x.status == 200) {
            console.log(x.responseText);
            products = JSON.parse(x.responseText);
            callback();
        }
        else {
            if (count <= 6) {
                console.log("Retrying");
                count++;
                request();
            }
            else {
                console.log("Try again later");
            }
        }
    }
    x.timeout = 200;
    x.ontimeout = function() {
        if (count <= 6) {
            console.log("Retrying");
            count++;
            request();
        }
        else {
            console.log("Try again later");
        }
    }
    x.onerror = function() {
         if (count <= 6) {
            console.log("Retrying");
            count++;
            request();
        }
        else {
            console.log("Try again later");
        }   
    }
}
var success = function() {
    console.log("Successfully received response");
}
request(success);


var cart = {
    'Box1': 0,
    'Box2': 0,
    'Clothes1': 0,
    'Clothes2': 0,
    'Jeans': 0,
    'Keyboard': 0,
    'KeyboardCombo': 0,
    'Mice': 0,
    'PC1': 0,
    'PC2': 0,
    'PC3': 0,
    'Tent': 0
}; //global variable products as an associative array

var productName;
var t = 0;
var elapsed = setInterval(inactive, 1000);
var inactiveTime = setInterval(timeOutPopup, 30000); //global variable inactiveTime with an initial value of 30000 milli seconds

document.addEventListener("DOMContentLoaded", hideRemove);

function inactive() {
    t++;
    if (t > 300000) t = 0;
    document.getElementById("elapsed").innerHTML = "Inactive Time: " + t;
}

function hideRemove() {
    var buttons = document.getElementsByClassName('remove');
    console.log(buttons.length);
    //buttons[1].className = 'hidden';
    for (i = 0; i < buttons.length; i++) {
        var pid = buttons[i].parentNode.id;
        //console.log(i);
        buttons[i].className += " " + "hidden";
        if (isNaN(cart[pid]) || cart[pid] == 0) {
            buttons[i].className += " " + "hidden";    
        }
        else {
            buttons[i].className = "remove";
        }
    }
}

function cartAdd(i) {
    var ind = i;
    return function() {
        addToCart(ind);
    }
}

function remCart(i) {
    var ind = i;
    return function() {
        removeFromCart(ind);
    }
}

function createTable() {
    var table = document.getElementById('tab');
    table.innerHTML = "";
    var totalValue = 0;
    for (i in cart) {
        if (cart[i] != 0) {
            console.log("i in cart is " + i);
            var tr = document.createElement('tr');
            var pname = document.createElement('td');
            var quantity = document.createElement('td');
            var totprice = document.createElement('td');
            //var td = document.createElement('td');
            var pb = document.createElement('button');
            var mb = document.createElement('button');
            
            
            var p = document.createTextNode(i);
            var q = document.createTextNode(cart[i]);
            var pr = document.createTextNode(products[i].price * cart[i]);
            totalValue += products[i].price * cart[i];
            var plus = document.createTextNode('+');
            var minus = document.createTextNode('-');
            
            pname.appendChild(p);
            quantity.appendChild(q);
            totprice.appendChild(pr);
            //td.appendChild(pb);
            //td.appendChild(mb);
            //td.onclick = console.log("Table data clicked!");
            pb.appendChild(plus);
            mb.appendChild(minus);
            var pbc = cartAdd(i);
            var nbc = remCart(i);
            pb.onclick = function() {
                pbc();
                createTable();
            }
            mb.onclick = function() {
                nbc();
                createTable();
            }
            
            tr.appendChild(pname);
            tr.appendChild(quantity);
            tr.appendChild(totprice);
            tr.appendChild(pb);
            tr.appendChild(mb);
            
            table.appendChild(tr);
        }
    }
    var cbrow = document.createElement('tr');
    var cb = document.createElement('button');
    var cbText = document.createTextNode('Checkout');    
    cb.appendChild(cbText);
    cbrow.appendChild(cb);
    cb.onclick = function() {
        var oldProducts = JSON.parse(JSON.stringify(products));        
        alert('Confirming total price and product availability');
        var func = function() {
                for (i in cart) {
                if (cart[i] != 0) {
                    console.log(i);
                    if (products[i].price != oldProducts[i].price) {
                        alert("Price for " + i + " has changed");
                        createTable();
                    }
                    if (products[i].quantity < cart[i]) {
                        cart[i] = products[i].quantity;
                        alert("Quantity for " + i + " has changed");
                        createTable();
                    }
                }
            }
        }
        request(func);        
    }
    table.appendChild(cbrow);
    document.getElementById('total').innerHTML = "Total: " + totalValue;
    //document.getElementById('overlay').appendChild(table);
}

function overlay() {
	el = document.getElementById("overlay");
	el.style.visibility = (el.style.visibility == "visible") ? "hidden" : "visible";
}

//
//for (i in buttons) {
//    this.className = 'hidden';
//}

function cartVal() {
    val = 0;
    console.log(val);
    for (i in cart) {
        val += products[i].price * cart[i];
    }
    console.log(val);
    document.getElementById("cartval").innerHTML = "Cart($" + val + ")";
}

/* time out pop up function */
function timeOutPopup() { 
    alert('Hey there! Are you still planning to buy something?');
	clearTimeout(inactiveTime);
    t = 0;	
	inactiveTime = setInterval(timeOutPopup, 30000);
};

/* check if the cart is empty */
function isEmpty(obj) {
    if (typeof obj == 'undefined' || obj === null || obj === '') 
		return true;
 
    if (typeof obj == 'number' && isNaN(obj)) 
		return true;
  
    return false;
}

function isEmptyObj (obj) {
	for (var i in obj) {
		if(obj.hasOwnProperty(i))
			return false;
		}
	return true;
	
};

/* add products to cart function */
function addToCart(productName) {
    t = 0;
    if (products[productName].quantity == 0) return;
    
    if (isEmpty(cart[productName])) {
        cart[productName] = 1;
		products[productName].quantity = products[productName].quantity - 1;
        console.log("Put " + productName + " inside cart");
        console.log("Products Array: ");
		console.log(products[productName]);
        console.log("Cart Array: ");
        console.log(cart[productName]);
    }
    else {
		cart[productName] = cart[productName] + 1;
		products[productName].quantity = products[productName].quantity - 1;
        console.log("Incremented " + productName + " in cart");
        console.log("Products Array: ");
		console.log(products[productName]);
        console.log("Cart Array: ");
        console.log(cart[productName]);
    }
//	if (products[productName].quantity <= 0)
//		delete products[productName];
    
    cartVal();
    hideRemove();
    clearInterval(inactiveTime);
    inactiveTime = setInterval(timeOutPopup, 30000);
};

/* remove products from cart function */
function removeFromCart(productName) {
	t = 0;
    if (cart[productName] <= 0) {
		delete cart[productName];		
		return;
	}
    else if (cart[productName] === 1) {
		products[productName].quantity = products[productName].quantity + 1;
        delete cart[productName];

        console.log("Deleted " + productName + "from cart");
    }
	else if (cart[productName] > 1) {
		cart[productName] = cart[productName] - 1;
		if (isEmpty(products[productName]))
			products[productName].quantity = 1;
		else {
			products[productName].quantity = products[productName].quantity + 1;
		}
        console.log("Decremented " + productName + "from cart");
    }
    cartVal();
    hideRemove();
	clearInterval(inactiveTime);
    inactiveTime = setInterval(timeOutPopup, 30000);
};

var items = []; var quantity = []; var ind = 0;

function showCart() {
    t = 0;
    for (i in cart) {
        console.log(i);
        items.push(i);
        quantity.push(cart[i]);
    }
    alertForCart();
    console.log("before alert: " + ind);
};

function alertForCart() {
    t = 0;
	if (isEmptyObj(cart))
		alert("Nothing in cart!");
	else {
		alert(items[ind] + " : " + quantity[ind]);
		ind++;
		console.log("after alert: " + ind);
		if (ind < items.length)
			setTimeout(alertForCart, 30000);
		else return; 
	};
};


