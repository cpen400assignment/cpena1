function addOrdersToTable(cartString, cartTotal){
	MongoClient.connect('mongodb://localhost:27017/cpendb', function(err, db) {
	    db.collection('orders').insert( { cart: cartString, total: cartTotal } )
	    db.close();
	  });
}

function decrementCount(cartString) {
  var cart = JSON.parse(cartString);
  console.log(cart);
  MongoClient.connect('mongodb://localhost:27017/cpendb', function(err, db) {
      for (e in cart){

        var qty = cart[e];
        console.log(e + " " + qty);
        db.collection('products').findAndModify(
            { "name": e },
            [],
            { $inc: { "quantity": -qty } },
            { upsert: true, new: true },
        function(err, doc){
          if(!err){
            console.log('the id is: '+ doc);
          } else {
            console.log(err)
          }
        })
        console.log('successfully updated');
      }
      // db.close();
    })
}