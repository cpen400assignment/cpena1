var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');

var routes = require('./routes/index');
var users = require('./routes/users');

var app = express();

var MongoClient = require('mongodb').MongoClient;
var mongo = require('mongodb');
var monk = require('monk');
//var db = monk('localhost:27017/cpendb');

var bodyParser = require('body-parser');

function addOrdersToTable(cartString, cartTotal){
  MongoClient.connect('mongodb://localhost:27017/cpendb', function(err, db) {
      db.collection('orders').insert( { cart: cartString, total: cartTotal } )
      db.close();
    });
}

function decrementCount(cartString) {
  var cart = JSON.parse(cartString);
  console.log(cart);
  MongoClient.connect('mongodb://localhost:27017/cpendb', function(err, db) {
      for (e in cart){

        var qty = cart[e];
        console.log(e + " " + qty);
        db.collection('products').findAndModify(
            { "name": e },
            [],
            { $inc: { "quantity": -qty } },
            { upsert: true, new: true },
        function(err, doc){
          if(!err){
            console.log('the id is: '+ doc);
          } else {
            console.log(err)
          }
        })
        console.log('successfully updated');
      }
      // db.close();
    })
}

app.set('port', (process.env.PORT || 5000))
app.use(express.static(__dirname + '/public'))
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.post('/addcart', function(req, res) {
  console.log("hehehe")
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  console.log(req.body)
  var cartJson = req.body.cart;
  var cartTotal = req.body.total;
  addOrdersToTable(cartJson, cartTotal);
  decrementCount(cartJson);
});



// set up connection
//var mongoclient = new MongoClient(new Server("localhost", 27017), {native_parser: true});
// view engine setup
var productcollection;
MongoClient.connect('mongodb://localhost:27017/cpendb', function(err, db) {
  if (err) {
    throw err;
  } else {
    console.log("successfully connected to cpendb");
    var collection = db.collection('products').find().toArray(function(err, items){
      console.log(items);
    });
    // console.log(collection.find()));
    //productcollection = db.collection('products');
    //console.log(productcollection.find({"name" : "products"}));
  }
});



// app.post('/addCart', function(req, res) {

// });

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// app.use(function(req, res, next) {
//   req.db() = db;
//   next();
// });

app.use('/', routes);
app.use('/users', users);
//app.use('/products', products);
//app.use('/orders', orders);

// app.get('/products', function(req, res) {
//     db.products.find({}, function(err, docs) {
//       if (err) res.json(err);
//       else res.send(db.products.find());
//     });
// });
// app.get('/products', function(req, res) {
//   mongoclient.open(function(err, mongoclient) {
//     var db = mongoclient.db("cpendb");
//     res.send(db.products.find());
//   })
// });

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
