/* 
CPEN 400 Assignment #2
Author: Brandon Nam, Shariq Aziz
Date: Oct. 15, 2015
*/

var cart = new Object(); //global variable cart as an associative array
var products = {Box1: "3", Box2: "3", Clothes1: "3", Clothes2: "3", Jeans: "3", Keyboard: "3", KeyboardCombo: "3", Mice: "3", PC1: "3", PC2: "3", PC3: "3", Tent: "3"}; //global variable products as an associative array



var productName;
var inactiveTime = setInterval(timeOutPopup, 30000); //global variable inactiveTime with an initial value of 30000 milli seconds


/* time out pop up function */
function timeOutPopup() { 
    alert('Hey there! Are you still planning to buy something?');
	clearTimeOut(inactiveTime);
	
	inactiveTime = setInterval(timeOutPopup, 30000);
};

/* check if the cart is empty */
function isEmpty(obj) {
    if (typeof obj == 'undefined' || obj === null || obj === '') 
		return true;
 
    if (typeof obj == 'number' && isNaN(obj)) 
		return true;
  
    return false;
}

function isEmptyObj (obj) {
	for (var i in obj) {
		if(obj.hasOwnProperty(i))
			return false;
		}
	return true;
	
};

/* add products to cart function */
function addToCart(productName) { 
    
    if (isEmpty(cart[productName])) {
        cart[productName] = 1;
		products[productName]--;
        console.log("Put " + productName + " inside cart");   
		console.log(products[productName]);
    }
    else {
		cart[productName] = cart[productName] + 1;
		products[productName] = products[productName] - 1;
        console.log("Incremented " + productName + " in cart");
		console.log(products[productName]);
    }
	if (products[productName] <= 0)
		delete products[productName];
    
    clearInterval(inactiveTime);
    inactiveTime = setInterval(timeOutPopup, 30000);
};

/* remove products from cart function */
function removeFromCart(productName) {
	
    if (cart[productName] <= 0) {
		delete cart[productName];		
		return;
	}
    else if (cart[productName] === 1) {
		products[productName] = products[productName] + 1;
        delete cart[productName];

        console.log("Deleted " + productName + "from cart");
    }
	else if (cart[productName] > 1) {
		cart[productName] = cart[productName] - 1;
		if (isEmpty(products[productName]))
			products[productName] = 1;
		else {
			products[productName] = products[productName] + 1;
		}
        console.log("Decremented " + productName + "from cart");
    }
	clearInterval(inactiveTime);
    inactiveTime = setInterval(timeOutPopup, 30000);
};

var items = []; var quantity = []; var ind = 0;

function showCart() {
    for (i in cart) {
        console.log(i);
        items.push(i);
        quantity.push(cart[i]);
    }
    alertForCart();
    console.log("before alert: " + ind);
};

function alertForCart() {
	if (isEmptyObj(cart))
		alert("Nothing in cart!");
	else {
		alert(items[ind] + " : " + quantity[ind]);
		ind++;
		console.log("after alert: " + ind);
		if (ind < items.length)
			setTimeout(alertForCart, 3000);
		else return; 
	};
};


